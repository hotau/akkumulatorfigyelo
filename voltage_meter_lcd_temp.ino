
#include <LiquidCrystal.h>
const int rs = 12, en = 11, d4 = 5, d5 = 4, d6 = 3, d7 = 2;
LiquidCrystal lcd(rs, en, d4, d5, d6, d7);

float input_volt = 0.0;
float temp=0.0;
float r1=100000.0;    //100000, 99000
float r2=10000.0;      //10000.0

// Hőmérő szenzor -------------------------------------------------------
int Vo;
float R1 = 10000;
float logR2, R2, T, Tc, Tf;
float c1 = 1.009249522e-03, c2 = 2.378405444e-04, c3 = 2.019202697e-07;

//-----------------------------------------------------------------------

void setup()
{
  pinMode(8, OUTPUT); // relé
   Serial.begin(9600);
   lcd.begin(16, 2);
   lcd.print("DC DIGI VOLTMETER");
}

void loop()
{
    int analogvalue = analogRead(A6);// volt meret bekötés
    input_volt = analogvalue*0.0048*3;  //0.004887
    
   Vo = analogRead(A7);// hőmérő bekötés
  R2 = R1 * (1023.0 / (float)Vo - 1.0);
  logR2 = log(R2);
  T = (1.0 / (c1 + c2*logR2 + c3*logR2*logR2*logR2));
  Tc = T - 273.15; 

   
if (input_volt < 0.1) 
   {
     input_volt=0.0;
   } 
    lcd.setCursor(0, 1);
    lcd.print("Voltage= ");
    lcd.print(input_volt);
    delay(300);


if(input_volt < 9 || input_volt > 15  || Tc > 30)
  {
  digitalWrite(8, HIGH);
  }
 Serial.print(Tc);
 Serial.print("  ");
 Serial.println(input_volt);
}
